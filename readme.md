# Dokumentace k projektu 1 do předmětu IPK

 | IPK   | Projekt 1    |
 | ------| ------------ |
 | jméno | Vít Solcčík  |
 | login | xsolci00     |
 | datum | 2017-03-18   |
 
### 1 Úvod
Jenoduchá klient server aplikace založena na RESTful API, komunikující pomocí HTTP.

### 2 Klient
Klient zpracuje argumenty a v případe jijch správnosti naváže spojení se serverem pomocí
BSD socketů. Samotný tok dat je jednoduchý TCP přenos.
Klientská aplikace může se serverovými soubory pracovat pomocí níze popsaných funkcí.

  - **put:** zkopíruje soubor na server
  - **get:** zkopíruje soubor ze serveru do aktuálního lokálního adresáře nebo na místo udané cestou
  - **del:** smaže soubor na serveru
  - **lst:** vypíše na standardní výstup obsah adresáře na serveru 
  - **mkd:** vytvoří adresář na serveru
  - **rmd:** odstraní adresář na serveru


### 3 Server
Každá z těchto funkcí vytvoří HTTP hlavičku pro server který na jejím základě spracuje požadavek.
V případě vyjímky (např.: kopírování neexistujícího souboru) server reaguje chybou,
jak na chybový výstup klienta tak i serveru a zasílá příslušnou hlavičku s chybou.

POZOR: Po každé operaci se server ukončí stejně jak klient a je nutné jej znovu spustit!!

### Příklady použití

Vytvoření adresáře bar na serveru bežícím na lokálním počítači a portu 12345:
$ ftrest mkd http://localhost:12345/tonda/foo/bar
 
Nahrání souboru doc.pdf na serveru do adresáře bar: 
$ ftrest put http://localhost:12345/tonda/foo/bar/doc.pdf ~/doc.pdf
  
Stažení souboru doc.pdf do lokálního adresáře:
$ ftrest get http://localhost:12345/tonda/foo/bar/doc.pdf
 
Odstranění souboru doc.pdf: 
$ ftrest del http://localhost:12345/tonda/foo/bar/doc.pdf
  
Odstranění adresáře bar:
$ ftrest rmd http://localhost:12345/tonda/foo/bar