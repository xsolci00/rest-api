

#include "ftrest.hpp"


using namespace std;

/**
 * @brief      { function_description }
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     { description_of_the_return_value }
 */
int main(int argc, char const *argv[])
{

    struct arguments arg = parseArguments(argc, argv);

    //Creating socket
    int client;
	int portNum = 6677;

    portNum = arg.port;

    client = socket(AF_INET, SOCK_STREAM, 0);

    if (client < 0)
    {
        cout << "Error establishing connection .. " << endl;
        exit(1);
    }

    //Connecting to server
    struct sockaddr_in server_addr;
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(portNum);


    if (connect(client,(struct sockaddr *)&server_addr, sizeof(server_addr)) == 0)
        cout << "Connection to the server port number: " << portNum << endl;
    else
        cerr << "Connection failed, fluidum missing" << endl;

    //Generate date and time for http
    time_t now = time(0);
    struct tm tm = *gmtime(&now);
    tm.tm_hour += GMT;
    strftime(arg.date, sizeof(arg.date), "%a, %d %b %Y %H:%M:%S %Z", &tm);

    //Communication with server
    do
    {
        if (arg.command == get)
        {
            send(client , "get" , sizeof(char)*4 , 0);
            get_func(client, arg);
        }

        if (arg.command == put)
        {
            send(client , "put" , sizeof(char)*4 , 0);
            put_func(client, arg);
        }

        if (arg.command == lst)
        {
            send(client , "lst" , sizeof(char)*4 , 0);
            lst_func(client, arg);
        }

        if (arg.command == mkd)
        {
            send(client , "mkd" , sizeof(char)*4 , 0);
            mkd_func(client, arg);
        }

        if (arg.command == rmd)
        {
            send(client , "rmd" , sizeof(char)*4 , 0);
            rmd_func(client, arg);
        }

        if (arg.command == del)
        {
            send(client , "del" , sizeof(char)*4 , 0);
            del_func(client, arg);
        }

    } while (0);

    //Communication over
    cout << "Connection terminated." << endl;
    close(client);
	return 0;

}


/**
 * @brief      Gets the function.
 *
 * @param[in]  client  The client
 * @param[in]  arg     The argument
 */
void get_func(int client, arguments arg)
{

    char buffer[BUF_SIZE];
    int read_size;
    int long size , recieved = 0;
    string find;

    //Concantenate path
    string path = string(arg.user) + "/" + string(arg.path);

    //Generate HTTP header
    sprintf(buffer, "GET %s?type=file HTTP/1.1\n"\
                    "Date: %s\n"\
                    "Accept: text/plain\n"\
                    "Accept-Encoding: identity\n\n",
            path.c_str(), arg.date);

    //Send HTTP header
    if (send(client, buffer, BUF_SIZE, 0) < 0)
        error_func(1); 

    // create new file
    FILE *write_file;
    string file;

    if (strcmp(arg.local,"./") == 0)    
        file = string(arg.local) + string(arg.file_folder);
    else
        file = string(arg.local);

    write_file = fopen(file.c_str(), "w");
    if (write_file == NULL)
    {
        cerr << "Local directory path error" << endl;
        exit(1);
    }
    //Send path (and file name)
    memset(buffer, 0, strlen(buffer));
    strcpy(buffer, path.c_str());
    send(client, buffer, BUF_SIZE, 0);
    memset(buffer, 0, strlen(buffer));

    // Recieve file size info
    recv(client, buffer, BUF_SIZE, 0);
    size = atoi(buffer);

    // write to file
    while ((recieved + BUF_SIZE) < size )
    {
        read_size = recv(client, buffer, BUF_SIZE, 0);
        fwrite(buffer, BUF_SIZE, 1, write_file);
        memset(buffer, 0, BUF_SIZE);
        recieved += BUF_SIZE;
    }
    if (recieved < size)
    {
        memset(buffer, 0, BUF_SIZE);
        read_size = recv(client, buffer, (size - recieved), 0);
        fwrite(buffer, (size - recieved), 1, write_file);  
    }

    // close the file
    fclose(write_file);

    // check status
    if (read_size == 0)
    {
        puts("Client disconnected");
        fflush(stdout);
    }
    else if (read_size == -1)
        perror("Recieve failed");

    //Recieve HTTP response
    memset(buffer, 0, BUF_SIZE);
    recv(client, buffer, BUF_SIZE, 0);
    find = string(buffer);

    if (find.find("HTTP/1.1 404 Not Found") != string::npos)
    {
        cerr << "File not found" << endl;
        //fclose(file.c_str());
        remove(file.c_str());
    }

}

/**
 * @brief      Puts a function.
 *
 * @param[in]  client  The client
 * @param[in]  arg     The argument
 */
void put_func(int client, arguments arg)
{
    char message[BUF_SIZE];
    char flag[4];
    struct stat file_stat;
    string size_str, find;
    stringstream strstream;
    long int sended = 0;
    
    //Concantenate path
    string path = string(arg.user) + "/" + string(arg.path);

    //Generate HTTP header
    sprintf(message,"PUT %s?type=file HTTP/1.1\n"\
                    "Date: %s\n"\
                    "Accept: text/plain\n"\
                    "Accept-Encoding: identity\n\n",
            path.c_str(), arg.date);

    //Send HTTP header
    if (send(client, message, BUF_SIZE, 0) < 0)
        error_func(1);

    // Open file
    FILE *file_read;
    file_read = fopen(arg.local, "r");
    if (file_read == NULL)
    {
        cerr << "File not exist."<< endl;
        exit(1);
    }

    // Send file path (name)
    memset(message, 0, strlen(message));
    strcpy(message, path.c_str());
    send(client, message, BUF_SIZE, 0);

    memset(message, 0, BUF_SIZE);
    recv(client, message, BUF_SIZE, 0);
    strcpy(flag ,message);

    if (strcmp(flag, "OK") == 0)
    {
        // Send file size
        stat(arg.local, &file_stat);
        strstream << file_stat.st_size;
        strstream >> size_str;
        memset(message, 0, strlen(message));
        strcpy(message, size_str.c_str());
        send(client, message, BUF_SIZE, 0);

        // Sending file
        while ((sended + BUF_SIZE) < file_stat.st_size)
        {
            // read BUF_SIZE bytes
            fread(message, BUF_SIZE, 1, file_read);
            // send BUF_SIZE bytes
            if ( send(client, message, BUF_SIZE, 0) < 0)
                puts("Send failed");
            
            sended += BUF_SIZE; 
            memset(message, 0, BUF_SIZE);
        }
        if (sended < file_stat.st_size)
        {
            memset(message, 0, BUF_SIZE);
            fread(message, (file_stat.st_size - sended), 1, file_read);
            if ( send(client, message, BUF_SIZE, 0) < 0)
                puts("Send failed");
        }
    }

    // Close file
    fclose(file_read);

    //Recieve HTTP response
    memset(message, 0, BUF_SIZE);
    recv(client, message, BUF_SIZE, 0);
    find = string(message);

    if (strcmp(flag, "NO") == 0)
        cerr << "Already exists." << endl;

    if (strcmp(flag, "NN") == 0)
        cerr << "Directory not found." << endl;

}

/**
 * @brief      { function_description }
 *
 * @param[in]  client  The client
 * @param[in]  arg     The argument
 */
void lst_func(int client, arguments arg)
{
    char buffer[BUF_SIZE];
    string find;

    //Concantenate path
    string path = string(arg.user) +'/'+ string(arg.path);

    //Generate HTTP header
    sprintf(buffer, "GET %s?type=folder HTTP/1.1\n"\
                    "Date: %s\n"\
                    "Accept: text/plain\n"\
                    "Accept-Encoding: identity\n\n",
            path.c_str(), arg.date);

    //Send HTTP header
    if (send(client, buffer, BUF_SIZE, 0) < 0)
        error_func(1); 

    
    // Send path
    if ( send(client , path.c_str() , strlen(path.c_str()) , 0) < 0)
        error_func(1);

    sleep(1);

    // Receive a message with list segments (ls) from server
    memset(buffer, 0, BUF_SIZE);
    recv(client , buffer , BUF_SIZE , 0);
    printf("%s\n", buffer);

    //send(client, buffer , BUF_SIZE, 0 );

    //Recieve HTTP response
    memset(buffer, 0, BUF_SIZE);
    recv(client, buffer, BUF_SIZE, 0);
    find = string(buffer);



    if (find.find("HTTP/1.1 404 Not Found") != string::npos)
        cerr << "Directory not found." << endl; 


    //recv(client, buffer, BUF_SIZE, 0);

}

/**
 * @brief      { function_description }
 *
 * @param[in]  client  The client
 * @param[in]  arg     The argument
 */
void mkd_func(int client, arguments arg)
{
    char buffer[BUF_SIZE];
    string find;
    // No path
    if(strlen(arg.path) == 0)
        error_func(1);

    //Concantenate path   
    string path = string(arg.user) +'/'+ string(arg.path);

    //Generate HTTP header
    sprintf(buffer, "PUT %s?type=folder HTTP/1.1\n"\
                    "Date: %s\n"\
                    "Accept: text/plain\n"\
                    "Accept-Encoding: identity\n\n",
            path.c_str(), arg.date);

    //Send HTTP header
    if (send(client, buffer, BUF_SIZE, 0) < 0)
        error_func(1); 

    //Send path to folder
    if ( send(client , path.c_str() , strlen(path.c_str()) , 0) < 0)
        cerr << "Uknown error" << endl;

    //Recieve HTTP response
    recv(client, buffer, BUF_SIZE, 0);
    find = string(buffer);

    if (find.find("HTTP/1.1 400 Bad Request") != string::npos)
        cerr << "Already exist." << endl;
}

/**
 * @brief      { function_description }
 *
 * @param[in]  client  The client
 * @param[in]  arg     The argument
 */
void rmd_func(int client, arguments arg)
{
    char buffer[BUF_SIZE];
    string find;
    // No path
    if(strlen(arg.path) == 0)
        error_func(1);

    //Concantenate path
    string path = string(arg.user) +'/'+ string(arg.path);

    //Generate HTTP header
    sprintf(buffer, "DELETE %s?type=folder HTTP/1.1\n"\
                    "Date: %s\n"\
                    "Accept: text/plain\n"\
                    "Accept-Encoding: identity\n\n",
            path.c_str(), arg.date);

    //Send HTTP header
    if (send(client, buffer, BUF_SIZE, 0) < 0)
        error_func(1); 

    //Send path to delete folder
    if ( send(client , path.c_str() , strlen(path.c_str()) , 0) < 0)
        error_func(1);


    //Recieve HTTP response
    recv(client, buffer, BUF_SIZE, 0);
    find = string(buffer);

    if (find.find("HTTP/1.1 404 Not Found") != string::npos)
        cerr << "Directory not found." << endl;

    if (find.find("HTTP/1.1 400 Bad Request") != string::npos)
        cerr << "Not a directory." << endl;

    if (find.find("HTTP/1.1 409 Conflict") != string::npos)
        cerr << "Directory not empty." << endl;
}

/**
 * @brief      { function_description }
 *
 * @param[in]  client  The client
 * @param[in]  arg     The argument
 */
void del_func(int client, arguments arg)
{

    char buffer[BUF_SIZE];
    string find;
    // No path
    if(strlen(arg.path) == 0)
        error_func(1);
    
    //Concantenate path
    string path = string(arg.user) +'/'+ string(arg.path);

    //Generate HTTP header
    sprintf(buffer, "DELETE %s?type=file HTTP/1.1\n"\
                    "Date: %s\n"\
                    "Accept: text/plain\n"\
                    "Accept-Encoding: identity\n\n",
            path.c_str(), arg.date);

    //Send HTTP header
    if (send(client, buffer, BUF_SIZE, 0) < 0)
        error_func(1);

    //Send path to delete file
    if ( send(client , path.c_str() , strlen(path.c_str()) , 0) < 0)
        error_func(1);

    //Recieve HTTP response
    recv(client, buffer, BUF_SIZE, 0);
    find = string(buffer);

    if (find.find("HTTP/1.1 404 Not Found") != string::npos)
        cerr << "File not found." << endl;

    if (find.find("HTTP/1.1 400 Bad Request") != string::npos)
        cerr << "Not a file." << endl;
}

/**
 * @brief      { function_description }
 *
 * @param[in]  code  The code
 */
void error_func(int code)
{
    cerr << "Uknown error." << endl;
    exit(code);
}