

#ifndef FTREST_H
#define FTREST_H

#include <iostream>
#include <string.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/stat.h>
#include <dirent.h>


#define	BUF_SIZE 1024
#define	CHUNK_SIZE 200
#define GMT (+1)

enum com
{
	del = 0,
	get = 1,
	put = 2,
	lst = 3,
	mkd = 4,
	rmd = 5,
};

typedef struct arguments
{
	int command;
	char remote[CHUNK_SIZE];
	char root[CHUNK_SIZE];
	char local[CHUNK_SIZE];
	char host[CHUNK_SIZE];
	char http[CHUNK_SIZE];
	char user[CHUNK_SIZE];
	char path[CHUNK_SIZE];
	char port_str[CHUNK_SIZE];
	char file_folder[CHUNK_SIZE];
	char file_client[CHUNK_SIZE];
	char date[CHUNK_SIZE];
	int port;
}arguments;


int init(struct sockaddr_in server_addr, int portNum);

arguments parseArguments(int argc, char const *argv[]);

void error_args(int code);

void get_func(int client, arguments arg);

void put_func(int client, arguments arg);

void lst_func(int client, arguments arg);

void mkd_func(int client, arguments arg);

void rmd_func(int client, arguments arg);

void del_func(int client, arguments arg);

void error_func(int code);

#endif