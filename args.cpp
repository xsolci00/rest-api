/**
 * @file    args.cpp
 * @author  VIT SOLCIK
 * @date:   2017-03-18
 * @brief   Simple REST client server application
 */


#include "ftrest.hpp"

using namespace std;


/**
 * @brief      { function_description }
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     { description_of_the_return_value }
 */
struct arguments parseArguments(int argc, char const *argv[])
{
    struct arguments arg;
    //Clear
    memset(arg.remote, 0, CHUNK_SIZE);
    memset(arg.root, 0, CHUNK_SIZE);
    memset(arg.local, 0, CHUNK_SIZE);
    memset(arg.host, 0, CHUNK_SIZE);
    memset(arg.http, 0, CHUNK_SIZE);
    memset(arg.user, 0, CHUNK_SIZE);
    memset(arg.path, 0, CHUNK_SIZE);
    memset(arg.port_str, 0, CHUNK_SIZE);
    memset(arg.file_folder, 0, CHUNK_SIZE);
    memset(arg.file_client, 0, CHUNK_SIZE);


    if (argc > 4 || argc < 3)
    {
        fprintf( stderr, "Wrong parametres!\n");
        exit(-1);
    }


    if (strcmp(argv[1],"del") == 0)
        arg.command = del;

    else if (strcmp(argv[1],"get") == 0)
        arg.command = get;

    else if (strcmp(argv[1],"put") == 0)
        arg.command = put;

    else if (strcmp(argv[1],"lst") == 0)
        arg.command = lst;

    else if (strcmp(argv[1],"mkd") == 0)
        arg.command = mkd;

    else if (strcmp(argv[1],"rmd") == 0)
        arg.command = rmd;
    else
    {
        fprintf( stderr, "Wrong parametres!\n");
        exit(-1);  
    }

    if ((arg.command == put) && argc != 4)
    {
        fprintf( stderr, "Wrong parametres!\n");
        exit(-1);
    }

    if (argc == 4)
    {
        strcpy(arg.local, argv[3]);
        strcpy(arg.remote, argv[2]);
    }
    else if (argc == 3)
    {
        strcpy(arg.remote, argv[2]);
    }
    else
    {
        fprintf( stderr, "Wrong parametres!\n");
        exit(-1); 
    }

    int count_full = strlen(arg.remote);
    int count = 0;
    int count2 = 0;
    int count3 = 0;
    int count4 = 0;
    int count5 = 0;
    int count6 = 0;
    int count7 = 0;
    int count8 = 0;
    //http^://localhost:portNum/user/foo/bar
    for (count; arg.remote[count] != ':' ; ++count)
    {;}
    strncpy(arg.http, arg.remote, count);
    arg.http[count] = '\0';

    //http://^localhost:portNum/user/foo/bar
    count += 3;
    
    //http://localhost^:portNum/user/foo/bar
    count2 = count;
    for (count2; arg.remote[count2] != ':' ; ++count2)
    {;}
    if ((count2 - count) < 1)
        error_args(1);
    strncpy(arg.host, &arg.remote[count], count2 - count);
    arg.host[count2 - count] = '\0';

    //http://localhost:^portNum/user/foo/bar
    if (arg.remote[count2] != ':')
        error_args(1);
    count2 += 1;

    //http://localhost:portNum^/user/foo/bar
    count3 = count2;
    for (count3; arg.remote[count3] != '/' ; ++count3)
    {;}
    if ((count3 - count2) < 1)
        error_args(1);
    strncpy(arg.port_str, &arg.remote[count2], count3 - count2);
    arg.port_str[count3 - count2] = '\0';

    //http://localhost:portNum/^user/foo/bar
    if (arg.remote[count3] != '/')
        error_args(2);
    count3 += 1;

    //http://localhost:portNum/user^/foo/bar
    count4 = count3;
    for (count4; ((arg.remote[count4] != '/') && (count4 < count_full)); ++count4)
    {;}      
    if ((count4 - count3) < 1)
        error_args(3);
    strncpy(arg.user, &arg.remote[count3], count4 - count3);
    arg.user[count4 - count3] = '\0';

    //http://localhost:portNum/user/^foo/bar
    // if (arg.remote[count4] != '/')
    //     error_args(4);
    count4 += 1;

    //http://localhost:portNum/user/foo/bar^
    count5 = count4;
    for (count5; arg.remote[count5] != '\0' && (count5 < count_full); ++count5)
    {;}
    // if ((count5 - count4) < 1)
    //     error_args(5);
    strncpy(arg.path, &arg.remote[count4], count5 - count4);
    arg.path[count5 - count4] = '\0';

    //http://localhost:portNum/user/foo/^bar^
    count6 = count5 = strlen(arg.remote);
    for (count6; (arg.remote[count6] != '/') && (count6 > 0); count6--)
    {;}
    count6++;
    strncpy(arg.file_folder, &arg.remote[count6], count5 - count6);


    //local default
    if (strcmp(arg.local, "") == 0)
    {
        strcpy(arg.local,"./");
        arg.local[2] = '\0';
    }
    else
    {//get name of local file
        count8 = count7 = strlen(arg.local);
        for (count7; arg.local[count7] != '/' && (count7 > 0) ; count7--)
        {;} count7++;
        strncpy(arg.file_client, &arg.local[count7], count8 - count7);
        arg.file_client[count8 - count7] = '\0';
    }

    arg.port = atoi(arg.port_str);


    // printf("arg.host: %s\n",arg.host );
    // printf("arg.port_str: %s\n",arg.port_str );
    // printf("arg.port: %d\n",arg.port );
    // printf("arg.user: %s\n",arg.user );
    // printf("arg.path: %s\n",arg.path );
    // printf("arg.file_folder: %s\n",arg.file_folder );
    // printf("arg.file_client: %s\n",arg.file_client );


    return arg;
}

/**
 * @brief      { function_description }
 *
 * @param[in]  code  The code
 */
void error_args(int code)
{
    cerr << "Wrong path format!" << endl;
    exit(code);
}