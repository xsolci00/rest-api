
#include <iostream>
#include <string.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>

#define	BUF_SIZE 1024
#define	CHUNK_SIZE 200
#define GMT (+1)


enum com
{
	del = 0,
	get = 1,
	put = 2,
	lst = 3,
	mkd = 4,
	rmd = 5,
}command;

struct arguments
{
	int command;
	char remote[CHUNK_SIZE];
	char root[CHUNK_SIZE];
	char local[CHUNK_SIZE];
	char host[CHUNK_SIZE];
	char http[CHUNK_SIZE];
	char user[CHUNK_SIZE];
	char path[CHUNK_SIZE];
	char port_str[CHUNK_SIZE];
	char date[CHUNK_SIZE];
	int port;
};


int init(struct sockaddr_in server_addr, int portNum);

struct arguments parseArguments(int argc, char const *argv[]);

int get_func(int server, arguments arg);

int put_func(int server, arguments arg);

void lst_func(int server, arguments arg);

int mkd_func(int server, arguments arg);

void rmd_func(int server, arguments arg);

int del_func(int server, arguments arg);