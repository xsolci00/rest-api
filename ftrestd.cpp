
#include "ftrestd.hpp"

using namespace std;


int main(int argc, char const *argv[])
{

	char buffer[BUF_SIZE];
	struct arguments arg = parseArguments(argc, argv);
	int client, server;
	int portNum = 6677;


	//Setting up port
	if (arg.port != 0)
		portNum = arg.port;

	//Init server and binding
	struct sockaddr_in server_addr;
	socklen_t size;

	client = init(server_addr , portNum);
    size = sizeof(server_addr);

    //Client connecting
    server = accept(client,(struct sockaddr *)&server_addr,&size);

	if (server < 0) 
		cout << "=> Error on accepting..." << endl;

    //Generate date and time for http
    time_t now = time(0);
    struct tm tm = *gmtime(&now);
    tm.tm_hour += GMT;
    strftime(arg.date, sizeof(arg.date), "%a, %d %b %Y %H:%M:%S %Z", &tm);

	//Recieve command

	recv(server, buffer, BUF_SIZE, 0);


	if (strcmp(buffer, "get") == 0)
		get_func(server, arg);

	else if (strcmp(buffer, "put") == 0)
		put_func(server, arg);

	else if (strcmp(buffer, "lst") == 0)
		lst_func(server, arg);

	else if (strcmp(buffer, "mkd") == 0)
		mkd_func(server, arg);

	else if (strcmp(buffer, "rmd") == 0)
		rmd_func(server, arg);

	else if (strcmp(buffer, "del") == 0)
		del_func(server, arg);

	memset(buffer, 0, BUF_SIZE);



    cout << "\n\n=> Connection terminated with IP " << inet_ntoa(server_addr.sin_addr) << endl;
    close(server);
    close(client);

	return 0;
}

/**
 * @brief      Gets the function.
 *
 * @param[in]  server  The server
 * @param[in]  arg     The argument
 */
int get_func(int server, arguments arg)
{
	char message[BUF_SIZE];
	char tmp[BUF_SIZE];
	struct stat file_stat;
	string size_str;
	stringstream strstream;
	long int sended = 0;
	string path, root;
	char ch;

	//recieve HTTP header
	recv(server, message, BUF_SIZE, 0);
	memset(message, 0, BUF_SIZE);

	// Recieve file and path
	recv(server, message, BUF_SIZE, 0);

    //root
    root = string(arg.root);
    ch = *root.rbegin();
    if ( (!root.empty()) && ( ch != '/'))
    	root.append("/");
    path = root + string(message);


	// Open file
	FILE *file_read;
	file_read = fopen(path.c_str(), "r");
	if (file_read == NULL)
	{
	    //Generate HTTP header
		sprintf(tmp, "HTTP/1.1 404 Not Found\n"\
	                	"Date: %s\n"\
	                	"Content-Type: text/plain\n"\
	                	"Content-Length: 0\n"\
	                	"Content-Encoding: identity\n\n",
	        	arg.date);
		send(server , tmp , BUF_SIZE , 0);
		send(server , tmp , BUF_SIZE , 0);

		return 1;
	}

    printf("message: %s\n",message );

	// Send file size
    stat(path.c_str(), &file_stat);
    strstream << file_stat.st_size;
    strstream >> size_str;
    memset(message, 0, strlen(message));
    strcpy(message, size_str.c_str());
	send(server, message, BUF_SIZE, 0);

	// Sending file
	while ((sended + BUF_SIZE) < file_stat.st_size)
	{
		// read BUF_SIZE bytes
		fread(message, BUF_SIZE, 1, file_read);
		// send BUF_SIZE bytes
		if ( send(server, message, BUF_SIZE, 0) < 0)
			puts("Send failed");
		
		sended += BUF_SIZE; 
		memset(message, 0, BUF_SIZE);
	}
	if (sended < file_stat.st_size)
	{
		memset(message, 0, BUF_SIZE);
		fread(message, (file_stat.st_size - sended), 1, file_read);
		if ( send(server, message, BUF_SIZE, 0) < 0)
			puts("Send failed");
	}
	
	// Close file
	fclose(file_read);

    //HTTP respond
	//Generate HTTP header
	sprintf(message,"HTTP/1.1 200 OK\n"\
                	"Date: %s\n"\
                	"Content-Type: text/plain\n"\
                	"Content-Length: 0\n"\
                	"Content-Encoding: identity\n\n",
        	arg.date);
	send(server , message , BUF_SIZE , 0);
	send(server , message , BUF_SIZE , 0);
	send(server , "42" , BUF_SIZE , 0);

}

/**
 * @brief      Gets the function.
 *
 * @param[in]  server  The server
 * @param[in]  arg     The argument
 */
int put_func(int server, arguments arg)
{
    char buffer[BUF_SIZE];
    int read_size;
    int long size , recieved = 0;
	string path, root;
	char ch;

	//recieve HTTP header
	recv(server, buffer, BUF_SIZE, 0);
	memset(buffer, 0, BUF_SIZE);

	// Recieve path (name) of file
	memset(buffer, 0, strlen(buffer));
	recv(server, buffer, BUF_SIZE, 0);

    //root
    root = string(arg.root);
    ch = *root.rbegin();
    if ( (!root.empty()) && ( ch != '/'))
    	root.append("/");
    path = root + string(buffer);

	// Open file
	FILE *write_file;

	write_file = fopen(path.c_str(), "r");
	if (write_file != NULL)
	{
		cerr << "Already exist"<< endl;
	    send(server, "NO", BUF_SIZE,0);
	    //Generate HTTP header
		sprintf(buffer, "HTTP/1.1 400 Bad Request\n"\
	                	"Date: %s\n"\
	                	"Content-Type: text/plain\n"\
	                	"Content-Length: 0\n"\
	                	"Content-Encoding: identity\n\n",
	        	arg.date);
		//Send header
		send(server , buffer , BUF_SIZE , 0);
		send(server , "42" , BUF_SIZE , 0);

		fclose(write_file);
		return 1;
	}
	else
	{
		write_file = fopen(path.c_str(), "w");
		if (write_file == NULL)
		{
		    send(server, "NN", BUF_SIZE,0);
		    //Generate HTTP header
			sprintf(buffer, "HTTP/1.1 409 Conflict\n"\
		                	"Date: %s\n"\
		                	"Content-Type: text/plain\n"\
		                	"Content-Length: 0\n"\
		                	"Content-Encoding: identity\n\n",
		        	arg.date);
			//Send header
			send(server , buffer , BUF_SIZE , 0);
			send(server , "42" , BUF_SIZE , 0);

			return 1;
		}
	}

	//ready to recieve
	send(server, "OK", BUF_SIZE,0);

    // Recieve file size info
    memset(buffer, 0, BUF_SIZE);
    recv(server, buffer, BUF_SIZE, 0);
    size = atoi(buffer);

    // write to file
    while ((recieved + BUF_SIZE) < size )
    {
        read_size = recv(server, buffer, BUF_SIZE, 0);
        fwrite(buffer, BUF_SIZE, 1, write_file);
        memset(buffer, 0, strlen(buffer));
        recieved += BUF_SIZE;
    }
    if (recieved < size)
    {
        memset(buffer, 0, strlen(buffer));
        read_size = recv(server, buffer, (size - recieved), 0);
        fwrite(buffer, (size - recieved), 1, write_file);  
    }

    // close the file
    fclose(write_file);

    // check status
    if (read_size == 0)
    {
        puts("Client disconnected");
        fflush(stdout);
    }
    else if (read_size == -1)
        perror("Recieve failed");

    //HTTP respond
	//Generate HTTP header
	sprintf(buffer, "HTTP/1.1 200 OK\n"\
                	"Date: %s\n"\
                	"Content-Type: text/plain\n"\
                	"Content-Length: 0\n"\
                	"Content-Encoding: identity\n\n",
        	arg.date);

	send(server , buffer , strlen(buffer) , 0);

}

/**
 * @brief      { function_description }
 *
 * @param[in]  server_addr  The server address
 * @param[in]  portNum      The port number
 *
 * @return     { description_of_the_return_value }
 */
int init(struct sockaddr_in server_addr, int portNum)
{
	// init socket
	int client;

	client = socket(AF_INET, SOCK_STREAM, 0);

	if (client < 0)
	{
		cout << "Error establishing connection .. " << endl;
		exit(1);
	}

	 cout << "Server Socket created .." << endl;

	 server_addr.sin_family = AF_INET;
	 server_addr.sin_addr.s_addr = htons(INADDR_ANY);
	 server_addr.sin_port = htons(portNum);

	 //binding socket

	if (bind(client, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0 )
	{
		cout << "Error binding socket .. " << endl;
		exit(1);
	}

 	listen(client, 1);

 	return client;
}

/**
 * @brief      { function_description }
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     { description_of_the_return_value }
 */
struct arguments parseArguments(int argc, char const *argv[])
{
	struct arguments arg;

	if (argc > 5 || argc == 2 || argc == 4)
	{
		cerr << "Wrong parametres!" << endl;
		exit(-1);
	}

	if (argc == 3 && strcmp(argv[1],"-r") == 0)
		strcpy(arg.root, argv[2]);
	
	else if (argc == 5 && strcmp(argv[3],"-r") == 0)
		strcpy(arg.root, argv[4]);
	else
		strcpy(arg.root, "");

	if (argc == 3 && strcmp(argv[1],"-p") == 0)
		arg.port = atoi(argv[2]);

	else if (argc == 5 && strcmp(argv[3],"-p") == 0)
		arg.port = atoi(argv[4]);
	else
		arg.port = 0;

	return arg;
}

/**
 * @brief      { function_description }
 *
 * @param[in]  server  The server
 * @param[in]  arg     The argument
 */
void lst_func(int server, arguments arg)
{
	char buffer[BUF_SIZE];
	string path, root;
	char ch;
	bool lst_flag = true;

	//recieve HTTP header
	recv(server, buffer, BUF_SIZE, 0);
	memset(buffer, 0, BUF_SIZE);

	// Recieve path
	recv(server, buffer, BUF_SIZE, 0);
    strcpy(arg.path, buffer);

    //root
    root = string(arg.root);
    ch = *root.rbegin();
    if ( (!root.empty()) && ( ch != '/'))
    	root.append("/");
    path = root + string(arg.path);

    //List directory
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir(path.c_str())) != NULL)
	{

		// clen message
		memset(buffer, 0, BUF_SIZE);
		/* print all the files and directories within directory */
		while ((ent = readdir (dir)) != NULL)
		{

			// ignor files "." and ".."
			if ((strcmp(ent->d_name, ".") != 0)  && (strcmp(ent->d_name, "..") != 0))
			{
				strcat(buffer, ent->d_name);
				strcat(buffer, " ");
			}
		}
		closedir (dir);
	} 
	else
	{
		cerr << "Directory not found" << endl;
		lst_flag = false;
		memset(buffer, 0, BUF_SIZE);
	}



	// send message with list segments (ls) to client
	if ( send(server , buffer , BUF_SIZE , 0) < 0)
	{
		puts("Unknown error");
		exit(1);
	}

	//sleep(1);
	//recv(server, buffer, BUF_SIZE, 0);



	//Send http respond
	memset(buffer, 0, BUF_SIZE);
	if (lst_flag == true)
	{
	    //Generate HTTP header
		sprintf(buffer, "HTTP/1.1 200 OK\n"\
	                	"Date: %s\n"\
	                	"Content-Type: text/plain\n"\
	                	"Content-Length: 0\n"\
	                	"Content-Encoding: identity\n\n",
	        	arg.date);
		//Send header
		send(server , buffer , BUF_SIZE , 0);
		send(server , "42" , BUF_SIZE , 0);
	}
	else
	{
	    //Generate HTTP header
		sprintf(buffer, "HTTP/1.1 404 Not Found\n"\
	                	"Date: %s\n"\
	                	"Content-Type: text/plain\n"\
	                	"Content-Length: 0\n"\
	                	"Content-Encoding: identity\n\n",
	        	arg.date);
		//Send header
		send(server , buffer , BUF_SIZE , 0);
	}
	cout << buffer;
}

/**
 * @brief      { function_description }
 *
 * @param[in]  server  The server
 * @param[in]  arg     The argument
 */
int mkd_func(int server, arguments arg)
{
	char buffer[BUF_SIZE];
	string path, root;
	char ch;

	//recieve HTTP header
	recv(server, buffer, BUF_SIZE, 0);
	memset(buffer, 0, BUF_SIZE);

	// Recieve path
	recv(server, buffer, BUF_SIZE, 0);
    strcpy(arg.path, buffer);

    //root
    root = string(arg.root);
    ch = *root.rbegin();
    if ( (!root.empty()) && ( ch != '/'))
    	root.append("/");
    path = root + string(arg.path);

    printf("path mkd: %s\n", path.c_str() );
    //Making folder
	mkdir(path.c_str(), 0777);

	switch (errno)
	{
		case EEXIST:
			cerr << "Already exists." << endl;

		    //Generate HTTP header
	    	sprintf(buffer, "HTTP/1.1 400 Bad Request\n"\
	                    	"Date: %s\n"\
	                    	"Content-Type: text/plain\n"\
	                    	"Content-Length: 0\n"\
	                    	"Content-Encoding: identity\n\n",
	            	arg.date);
	    	//Send header
	    	send(server , buffer , strlen(buffer) , 0);

	    	return 1;
			break;
	}

    //Generate HTTP header
	sprintf(buffer, "HTTP/1.1 200 OK\n"\
                	"Date: %s\n"\
                	"Content-Type: text/plain\n"\
                	"Content-Length: 0\n"\
                	"Content-Encoding: identity\n\n",
        	arg.date);
	//Send header
	send(server , buffer , strlen(buffer) , 0);	

	return 0;
}

/**
 * @brief      { function_description }
 *
 * @param[in]  client_sock  The client sock
 * @param[in]  a            { parameter_description }
 */
void rmd_func(int server, arguments arg)
{

	char buffer[BUF_SIZE];
	string path, root;
	char ch;
	bool rmd_flag = true;

	//recieve HTTP header
	recv(server, buffer, BUF_SIZE, 0);
	memset(buffer, 0, BUF_SIZE);

	//recieving path
	recv(server, buffer, BUF_SIZE, 0);
    strcpy(arg.path, buffer);

    //root
    root = string(arg.root);
    ch = *root.rbegin();
    if ( (!root.empty()) && ( ch != '/'))
    	root.append("/");
    path = root + string(arg.path);

	//Deleting
	rmdir(path.c_str());

	switch (errno)
	{
		case ENOTEMPTY:
			cerr << "Directory not empty." << endl;
			rmd_flag = false;

		    //Generate HTTP header
	    	sprintf(buffer, "HTTP/1.1 409 Conflict\n"\
	                    	"Date: %s\n"\
	                    	"Content-Type: text/plain\n"\
	                    	"Content-Length: 0\n"\
	                    	"Content-Encoding: identity\n\n",
	            	arg.date);
	    	//Send header
	    	send(server , buffer , strlen(buffer) , 0);

			break;
		
		case ENOTDIR:
			cerr << "Not a directory." << endl;
			rmd_flag = false;

		    //Generate HTTP header
	    	sprintf(buffer, "HTTP/1.1 400 Bad Request\n"\
	                    	"Date: %s\n"\
	                    	"Content-Type: text/plain\n"\
	                    	"Content-Length: 0\n"\
	                    	"Content-Encoding: identity\n\n",
	            	arg.date);
	    	//Send header
	    	send(server , buffer , strlen(buffer) , 0);

			break;
		
		case ENOENT:
			cerr << "Directory not found." << endl;
			rmd_flag = false;

		    //Generate HTTP header
	    	sprintf(buffer, "HTTP/1.1 404 Not Found\n"\
	                    	"Date: %s\n"\
	                    	"Content-Type: text/plain\n"\
	                    	"Content-Length: 0\n"\
	                    	"Content-Encoding: identity\n\n",
	            	arg.date);
	    	//Send header
	    	send(server , buffer , strlen(buffer) , 0);
			break;
	}

	if (rmd_flag == true)
	{
	    //Generate HTTP header
    	sprintf(buffer, "HTTP/1.1 200 OK\n"\
                    	"Date: %s\n"\
                    	"Content-Type: text/plain\n"\
                    	"Content-Length: 0\n"\
                    	"Content-Encoding: identity\n\n",
            	arg.date);
    	//Send header
    	send(server , buffer , strlen(buffer) , 0);	
	}
}

/**
 * @brief      { function_description }
 *
 * @param[in]  server  The server
 * @param[in]  arg     The argument
 */
int del_func(int server, arguments arg)
{

	char buffer[BUF_SIZE];
	bool rm_flag = true;
	FILE * file;
	string path, root;
	char ch;

	//recieve HTTP header
	recv(server, buffer, BUF_SIZE, 0);
	memset(buffer, 0, BUF_SIZE);

	//recieve path
	recv(server, buffer, BUF_SIZE, 0);
    strcpy(arg.path, buffer);

    //root
    root = string(arg.root);
    ch = *root.rbegin();
    if ( (!root.empty()) && ( ch != '/'))
    	root.append("/");
    path = root + string(arg.path);


    //testing and deleting
	if (access (path.c_str(), F_OK) != 0)
	{
		rm_flag = false;
		cerr << "File not found." << endl;
	    //Generate HTTP header
    	sprintf(buffer, "HTTP/1.1 404 Not Found\n"\
                    	"Date: %s\n"\
                    	"Content-Type: text/plain\n"\
                    	"Content-Length: 0\n"\
                    	"Content-Encoding: identity\n\n",
            	arg.date);
    	//Send header
    	send(server , buffer , strlen(buffer) , 0);
		return 1;
	}


    file = fopen(path.c_str(), "w");

	switch (errno)
	{
		case EISDIR:
			cerr << "Not a file." << endl;
			rm_flag = false;

		    //Generate HTTP header
	    	sprintf(buffer, "HTTP/1.1 400 Bad Request\n"\
	                    	"Date: %s\n"\
	                    	"Content-Type: text/plain\n"\
	                    	"Content-Length: 0\n"\
	                    	"Content-Encoding: identity\n\n",
	            	arg.date);

	    	send(server , buffer , strlen(buffer) , 0);
			break;
	}

	if (file != NULL)
		fclose(file);


	if (rm_flag == true)
	{
	    //Generate HTTP header
		sprintf(buffer, "HTTP/1.1 200 OK\n"\
	                	"Date: %s\n"\
	                	"Content-Type: text/plain\n"\
	                	"Content-Length: 0\n"\
	                	"Content-Encoding: identity\n\n",
	        	arg.date);

		send(server , buffer , strlen(buffer) , 0);
		unlink(path.c_str());
	}
}